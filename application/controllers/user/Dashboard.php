<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	var $API;

    public function __construct()
	{
		parent::__construct();
		$this->API= $this->general->cekAPI();
		$this->load->model('user/m_dashboard','md');
		$this->general->cekUserLogin();
	}

	public function index()
	{
		$data['countArticle'] = $this->md->countArticle()->num_rows();
		$data['countNews'] = $this->md->countNews()->num_rows();
		$data['countTips'] = $this->md->countTips()->num_rows();
		$this->load->view('user/dashboard',$data);	
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/user/Dashboard.php */