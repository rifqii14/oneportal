<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller {

	var $API;
	
	public function __construct()
	{
		parent::__construct();
		$this->general->cekUserLogin();
		$this->API=$this->general->cekAPI();
	}

	public function index()
	{
		$data['datausers'] = json_decode($this->curl->simple_get($this->API.'/users'));
		$data['datacategories'] = json_decode($this->curl->simple_get($this->API.'/categories'));
		$data['datatype'] = json_decode($this->curl->simple_get($this->API.'/type'));
		$data['datanews'] = json_decode($this->curl->simple_get($this->API.'/news/user?id_user='.$this->session->userdata('id_user')));
		
		$this->load->view('user/articles',$data);
	}


	public function create()
	{
		if(isset($_POST['submit']))
		{
			$data['id_news'] = $this->input->post('id_news');
			$data['id_categories'] = $this->input->post('id_categories');
			$data['id_type'] = $this->input->post('id_type');
			$data['id_user'] = $this->input->post('id_user');
			$data['title'] = $this->input->post('title');
			$data['content'] = $this->input->post('content');
			// $data['created_at'] = date('Y-m-d H:i:s');

			$add = $this->curl->simple_post($this->API.'/news', $data, array(CURLOPT_BUFFERSIZE => 10)); 

            redirect('user/Articles');
		} 
		else{
            $this->load->view('user/articles');
        }
	}

		public function update()
		{
			if(isset($_POST['submit']))
			{
				$data['id_news'] = $this->input->post('id_news');
				$data['id_categories'] = $this->input->post('id_categories');
				$data['id_type'] = $this->input->post('id_type');
				$data['title'] = $this->input->post('title');
				$data['content'] = $this->input->post('content');

				$add = $this->curl->simple_put($this->API.'/news', $data, array(CURLOPT_BUFFERSIZE => 10)); 

	            redirect('user/Articles/detail?id='.$data['id_news']);
			} 
			else{
	            $this->load->view('user/articles_detail');
	        }
		}

	public function active()
	{
		$id = $this->uri->segment(4);
		$data['id_news'] = $id;

		$update =  $this->curl->simple_put($this->API.'/news/active', $data, array(CURLOPT_BUFFERSIZE => 10)); 

		redirect('user/Articles');
	}

	public function deactive()
	{
		$id = $this->uri->segment(4);
		$data['id_news'] = $id;

		$update =  $this->curl->simple_put($this->API.'/news/deactive', $data, array(CURLOPT_BUFFERSIZE => 10)); 

		redirect('user/Articles');
	}

	public function delete()
	{

		$id = $this->uri->segment(4);
		$data['id_news'] = $id;
		// $data['deleted_at'] = date('Y-m-d H:i:s');

		$update =  $this->curl->simple_put($this->API.'/news/del', $data, array(CURLOPT_BUFFERSIZE => 10)); 

		redirect('user/Articles');
	}

	public function detail()
	{
		$data['datausers'] = json_decode($this->curl->simple_get($this->API.'/users'));
		$data['datacategories'] = json_decode($this->curl->simple_get($this->API.'/categories'));
		$data['datatype'] = json_decode($this->curl->simple_get($this->API.'/type'));		
		$data['datanews'] = json_decode($this->curl->simple_get($this->API."/news/news?id_news=".$_GET['id']));

		$this->load->view('user/articles_detail',$data);
	}
}

/* End of file Articles.php */
/* Location: ./application/controllers/user/Articles.php */