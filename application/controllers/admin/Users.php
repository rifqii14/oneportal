<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	var $API;
	
	public function __construct()
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->API=$this->general->cekAPI();
	}

	public function index()
	{
		$data['datausers'] = json_decode($this->curl->simple_get($this->API.'/users'));
		$this->load->view('admin/users',$data);
	}

	public function create()
	{
		if(isset($_POST['submit']))
		{
			$data['id_user'] = $this->input->post('id_user');
			$data['name'] = $this->input->post('name');
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$data['privileges'] = $this->input->post('privileges');
			// $data['created_at'] = date('Y-m-d H:i:s');

			$add = $this->curl->simple_post($this->API.'/users', $data, array(CURLOPT_BUFFERSIZE => 10)); 

            redirect('admin/Users');
		} 
		else{
            $this->load->view('admin/users');
        }
	}

	public function delete()
	{

		$id = $this->uri->segment(4);
		$data['id_user'] = $id;
		$data['deleted_at'] = date('Y-m-d H:i:s');

		$update =  $this->curl->simple_put($this->API.'/users/del', $data, array(CURLOPT_BUFFERSIZE => 10)); 

		redirect('admin/Users');
	}

	public function update()
	{
		if(isset($_POST['submit']))
		{
			$data['id_user'] = $this->input->post('id_user');
			$data['name'] = $this->input->post('name');
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$data['privileges'] = $this->input->post('privileges');
			// $data['modified_at'] = date('Y-m-d H:i:s');

			$update =  $this->curl->simple_put($this->API.'/users', $data, array(CURLOPT_BUFFERSIZE => 10)); 

			redirect('admin/Users');
		}
		else {
		    $this->load->view('admin/uategories');
		}
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/admin/Users.php */