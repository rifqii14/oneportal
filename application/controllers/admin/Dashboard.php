<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	var $API;

    public function __construct()
	{
		parent::__construct();
		$this->API= $this->general->cekAPI();
		$this->load->model('admin/m_dashboard','md');
		$this->general->cekAdminLogin();
	}
	
	public function index()
	{	
		$data['user'] = json_decode($this->curl->simple_get($this->API.'/users'));
		$data['countArticle'] = $this->md->countArticle()->num_rows();
		$data['countUser'] = $this->md->countUser()->num_rows();
		$data['countNews'] = $this->md->countNews()->num_rows();
		$data['countTips'] = $this->md->countTips()->num_rows();
		$this->load->view('admin/dashboard',$data);	
	}
}

/* End of file c_adashboard.php */
/* Location: ./application/controllers/admin/c_adashboard.php */