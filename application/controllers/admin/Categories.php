<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	var $API;

	public function __construct()
	{
		parent::__construct();
		$this->general->cekAdminLogin();
		$this->API=$this->general->cekAPI();
	}

	public function index()
	{
		$data['datacategories'] = json_decode($this->curl->simple_get($this->API.'/categories'));
		$this->load->view('admin/categories',$data);
	}

	public function create()
	{
		if(isset($_POST['submit']))
		{
			$data['id_categories'] = $this->input->post('id_categories');
			$data['categories'] = $this->input->post('categories');
			// $data['created_at'] = date('Y-m-d H:i:s');

			$add = $this->curl->simple_post($this->API.'/categories', $data, array(CURLOPT_BUFFERSIZE => 10)); 

            redirect('admin/Categories');
		} 
		else{
            $this->load->view('admin/categories');
        }
	}

	public function delete($id)
	{
		if(empty($id)){
			redirect('admin/Categories');
		}
		else {
			$del = $this->curl->simple_delete($this->API.'/categories', array('id_categories'=>$id), array(CURLOPT_BUFFERSIZE => 10)); 
			redirect('admin/Categories');
		}

	}

	public function update()
	{
		if(isset($_POST['submit']))
		{
			$data['id_categories'] = $this->input->post('id_categories');
			$data['categories'] = $this->input->post('categories');
			// $data['modified_at'] = date('Y-m-d H:i:s');

			$update =  $this->curl->simple_put($this->API.'/categories', $data, array(CURLOPT_BUFFERSIZE => 10)); 

			redirect('admin/Categories');
		}
		else {
		    $this->load->view('admin/categories');
		}
	}

}

/* End of file Categories.php */
/* Location: ./application/controllers/admin/Categories.php */