<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User');
//        for
    }

    function login() {
   
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == TRUE) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user = $this->User->checkLogin($username, $password);

            if (!empty($user)) {
                $sessionData['id_user'] = $user['id_user'];
                $sessionData['username'] = $user['username'];
                $sessionData['name'] = $user['name'];
                $sessionData['privileges'] = $user['privileges'];
                $sessionData['is_login'] = TRUE;

                $this->session->set_userdata($sessionData);

                if ($this->session->userdata('privileges') == 'Admin') {
                    $this->session->sess_expiration = 14400;
                    redirect('admin/Dashboard');
                } 
                else if ($this->session->userdata('privileges') == 'User') {
                    $this->session->sess_expiration = 14400;
                    redirect('user/Dashboard');
                } 
                // else if ($this->session->userdata('privileges') == 'Kasir'){
                //     $this->session->sess_expiration = 1;
                //     redirect('kasir/Penjualan');
                // }else{
                //     $this->session->sess_expiration = 14400;
                //     redirect('petugas/Dashboard');
                // }
            }else{
            echo "<script>
                alert('Username/Password Salah atau Akun tidak aktif');
                </script>";
            }
        }

        $this->load->view('users/login');
    }

    function logout() {

        $this->session->sess_destroy();
        redirect('users/login');
    }

}

?>
