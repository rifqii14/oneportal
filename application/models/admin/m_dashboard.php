<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	public function countArticle()
	{	
		$this->db->select('tbl_news.id_news');
		$this->db->from('tbl_news');
		$this->db->where('deleted_at',null);
	   	return $this->db->get();
	}

	public function countUser()
	{
		$this->db->select('tbl_user.id_user');
		$this->db->from('tbl_user');
		$this->db->where('deleted_at',null);
	   	return $this->db->get();
	}

	public function countNews()
	{	
		$this->db->select('tbl_news.id_news');
		$this->db->from('tbl_news');
		$this->db->where('id_type', 1);
		$this->db->where('deleted_at',null);
	   	return $this->db->get();
	}

	public function countTips()
	{	
		$this->db->select('tbl_news.id_news');
		$this->db->from('tbl_news');
		$this->db->where('id_type', 2);
		$this->db->where('deleted_at',null);
	   	return $this->db->get();
	}


}

/* End of file m_dashboard.php */
/* Location: ./application/models/admin/m_dashboard.php */