<?php 
$this->load->view('layout/template_head_start');
?>
<script src="<?php echo base_url('assets/js/core/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/plugins/chartjsv2/Chart.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/filesaver/FileSaver.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/canvas-blob/canvas-toBlob.js')?>"></script>
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Dashboard Admin
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
	<div class="row text-uppercase">
	    <div class="col-xs-6 col-sm-3">
	        <div class="block block-rounded">
	            <div class="block-content block-content-full">
	                <div class="font-s12 font-w700"> <i class="fa fa-newspaper-o"></i>  Total Article (All Type)</div>
	                <a class="h2 font-w300 text-primary"><?php echo $countArticle?></a>
	            </div>
	        </div>
	    </div>
	    <div class="col-xs-6 col-sm-3">
	        <div class="block block-rounded">
	            <div class="block-content block-content-full">
	                <div class="font-s12 font-w700"> <i class="fa fa-newspaper-o"></i>  Total Article (Tips & Trick)</div>
	                <a class="h2 font-w300 text-primary"><?php echo $countTips?></a>
	            </div>
	        </div>
	    </div>
	    <div class="col-xs-6 col-sm-3">
	        <div class="block block-rounded">
	            <div class="block-content block-content-full">
	                <div class="font-s12 font-w700"> <i class="fa fa-newspaper-o"></i>  Total Article (News)</div>
	                <a class="h2 font-w300 text-primary"><?php echo $countNews?></a>
	            </div>
	        </div>
	    </div>
	    <div class="col-xs-6 col-sm-3">
	        <div class="block block-rounded">
	            <div class="block-content block-content-full">
	                <div class="font-s12 font-w700"> <i class="fa fa-user"></i> Total User</div>
	                <a class="h2 font-w300 text-primary"><?php echo $countUser?></a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
$this->load->view('layout/template_footer_end.php');
?>