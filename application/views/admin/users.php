<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
               Users Data
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Dynamic Table Full -->
<div class="block">
    <div class="block-header">
        
    </div>
    <div class="block-content">
        <p class="text-muted font-13 m-b-30">
          <button id="addBtn" style="width:100px;" class="btn btn-success btn-block"><span class="icon-plus3"></span> Add Data</button>
        </p>
    <div class="table-responsive">
<?php if(!empty($datausers)){ ?>
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center" width="7%">No.</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Privileges</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php $no = 1; foreach($datausers as $row) { ?>
                <tr>
                    <td class="text-center" width="10%"><?php echo $no++?></td>
                    <td class="font-w600"><?php echo $row->name ?></td>
                    <td class="font-w600"><?php echo $row->username ?></td>
                    <td class="font-w600"><?php echo $row->privileges ?></td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a 
                            data-id-user="<?php echo $row->id_user ?>"
                            data-name="<?php echo $row->name ?>"
                            data-username="<?php echo $row->username ?>"
                            data-password="<?php echo $row->password ?>"
                            data-toggle="modal" data-target="#modal-data">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" data-toggle="modal" data-target="#modal-data" title="Edit"><i class="fa fa-pencil"></i></button>
                            </a>
                            <a href="<?= base_url().'admin/Users/delete/'.$row->id_user ?>" class="hapus">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>
                            </a>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
<?php } else { ?>
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center" width="7%">No.</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Privileges</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
<?php } ?>

    </div>
    </div>
</div>
<!-- END Dynamic Table Full -->

<!-- Modal Edit Data -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="modal-data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
            <ul class="block-options">
                <li>
                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Form Users</h3>
        </div>
            <form class="js-validation-bootstrap form-horizontal" method="post" enctype="multipart/form-data" role="form" id="form">
             <div class="block-content">
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Name</label>
                         <div class="col-lg-10">
                          <input type="text" id="id_user" name="id_user" hidden>
                          <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Username</label>
                         <div class="col-lg-10">
                          <input type="text" class="form-control" id="username" name="username" placeholder="Userame">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Password</label>
                         <div class="col-lg-10">
                          <input type="text" class="form-control" id="vpassowrd" name="password" placeholder="Password">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Privileges</label>
                         <div class="col-lg-10">
                          <select class="js-select2 form-control" id="privileges" name="privileges">
                            <option value="Admin">Admin</option>
                            <option value="User">User</option>
                          </select>
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button class="btn btn-primary" type="submit" name="submit" id="saveBtn"> Save&nbsp;</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                 </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Data -->
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>

<!-- Page JS Code -->
<style type="text/css">
	.my-error-class {
	    color:red;
	}
</style>
<script>

	$('#addBtn').on('click', function(e){	
		e.preventDefault();
		var modal = $('#modal-data')

    modal.find('#id_user').attr("value","");
    modal.find('#name').attr("value","");
    modal.find('#username').attr("value","");
    modal.find('#password').attr("value","");
		modal.modal('show');
		$('.block-title').text('Add User');
		$('#form').attr('action', "<?php echo base_url('admin/Users/create'); ?>").submit();

	});

	$('#modal-data').on('show.bs.modal', function (e) {
	    var div = $(e.relatedTarget)
	    var modal = $(this)
	    $('.block-title').text('Edit User');
	    modal.find('#id_user').attr("value",div.data('id-user'));
	    modal.find('#name').attr("value",div.data('name'));
      modal.find('#username').attr("value",div.data('username'));
      modal.find('#password').attr("value",div.data('password'));
	    $('#form').attr('action', "<?php echo base_url('admin/Users/update'); ?>").submit();
	});


	$('#saveBtn').on('click', function(e){	

		  var validator = $("#form").validate({
		  	errorClass: "my-error-class",
		    rules: {
			  name: {required: true},
        username: {required: true},
        password: {required: true, minlength: 4},
        privileges: {required: true}
		    },
		    messages: {
		      name: {required: "Name can't be empty"},
          username: {required: "Username can't be empty"},
          password: {required: "Password can't be empty", minlength: "Minimal 4 characters"},
          privileges: {required: "Please Select Privileges"}
		    }
		  });

	});

    $('.hapus').on("click", function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      swal({
          title: "Yakin Ingin Hapus?",
          text: "Data yang sudah dihapus tidak dapat dikembalikan!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Ya',
          cancelButtonText: "Tidak",
          confirmButtonClass: "btn-danger",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            swal("Berhasil!", "Data berhasil dihapus!", "success");
            window.location.replace(url);
          } else {
            swal("Batal!", "Data tidak jadi terhapus!", "error");
          }
        });
    });


        //select2
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            App.initHelpers('select2');
        });


</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>