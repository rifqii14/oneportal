<?php 
$this->load->view('layout/template_head_start');
$this->load->view('layout/template_head_end.php');
?>
<!-- Page Content -->
<div class="content content-boxed">
    <div class="block">
        <div class="block-content block-content-full block-content-narrow">
            <div class="text-center">
	            <span class="h4 font-w600 sidebar-mini-hide"><img width="300px" height="25%" src=<?php echo base_url("assets/img/oneBook1.png") ?>></span> <p></p>
	            <small class="text-muted text-center">Version 1.0.0</small><p></p>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                        <div class="panel-body">
                            <p> Toko Buku OneBook dikenal masyarakat sebagai jaringan toko buku yang terluas, terbesar dan terlengkap di Indonesia. Sampai pada saat ini, Kami telah bekerja sama dengan sebagian besar Penerbit dari seluruh Indonesia dan terus mengembangkan kerjasama dengan berbagai perusahaan untuk melengkapi produk yang dijual. </p>

                            <p>Toko Buku OneBook menggunakan sistem aplikasi POS (Point Of Sale) atau sistem kasir untuk mempermudah transaksi pembayaran. Aplikasi ini dibangun sendiri oleh Toko Buku OneBook dan sudah digunakan. Kedepannya aplikasi ini akan dikembangkan agar dapat lebih baik lagi.</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->

<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="text-center">
        OneBook by Rifqi Maulatur &copy;
    </div>
</footer>
<?php 
$this->load->view('layout/template_footer_start.php');
$this->load->view('layout/template_footer_end.php');
?>