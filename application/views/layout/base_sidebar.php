<?php  

if ($this->session->userdata('privileges') == 'Admin'){
    $c = "";
    $d = "hidden";
    $e = "hidden";
}
else if ($this->session->userdata('privileges') == 'User'){
    $c = "hidden";
    $d = "" ;
    $e = "hidden";
}
else{
    $c = "hidden";
    $d = "hidden" ;
    $e = "";
}

?>

            <!-- Sidebar -->
            <nav id="sidebar">

                <div id="sidebar-scroll">
                    
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                        <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                            <i class="fa fa-times"></i>
                        </button>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                <li class = "<?php echo $c; ?>">
                                    <a class=" <?php echo $c; ?><?=(current_url()==base_url('admin/Dashboard')) ? 'active':''?>" href="<?php echo site_url('admin/Dashboard') ?>"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>


                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Main Navigation</span></li>
                                <li class = "<?php echo $c; ?>">
                                    <a class="<?=(current_url()==base_url('admin/Articles')) ? 'active':''?>" href="<?php echo site_url('admin/Articles') ?>"><i class="fa fa-newspaper-o"></i><span class="sidebar-mini-hide">Management Article</span></a>
                                </li>
                                <li class = "<?php echo $c; ?>">
                                    <a class="<?=(current_url()==base_url('admin/Categories')) ? 'active':''?>" href="<?php echo site_url('admin/Categories') ?>"><i class="fa fa-list-ul"></i><span style="font-size: 12.5px" class="sidebar-mini-hide">Management Categories</span></a>
                                </li>
                                <li class = "<?php echo $c; ?>">
                                    <a class="<?=(current_url()==base_url('admin/Users')) ? 'active':''?>" href="<?php echo site_url('admin/Users') ?>"><i class="fa fa-user"></i><span class="sidebar-mini-hide">Management User</span></a>
                                </li>

                                <!-- SIDEBAR USER -->
                                <li class = "<?php echo $d; ?>">
                                    <a class=" <?php echo $d; ?><?=(current_url()==base_url('user/Dashboard')) ? 'active':''?>" href="<?php echo site_url('user/Dashboard') ?>"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>

                                <li class = "<?php echo $d; ?>">
                                    <a class="<?=(current_url()==base_url('user/Articles')) ? 'active':''?>" href="<?php echo site_url('user/Articles') ?>"><i class="fa fa-newspaper-o"></i><span class="sidebar-mini-hide">Management Article</span></a>
                                </li>

                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->