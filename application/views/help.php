<?php 
$this->load->view('layout/template_head_start');
$this->load->view('layout/template_head_end.php');
?>
<!-- Page Content -->
<div class="content content-boxed">
    <div class="block">
        <div class="block-content block-content-full block-content-narrow">
            <!-- Introduction -->
            <h2 class="h3 font-w600 push-30-t push text-center">BANTUAN</h2>
            <div id="faq1" class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q1">Bagaimana Cara Mendapatkan Akun?</a>
                        </h3>
                    </div>
                    <div id="faq1_q1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p>Hubungi Admin untuk memperbanyak akun. Satu akun hanya mempunyai satu akses, akses tersebut adalah Admin, Kasir dan Petugas</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q2">Apa itu Akses sebagai Admin?</a>
                        </h3>
                    </div>
                    <div id="faq1_q2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Jika akun tersebut mempunyai Akses sebagai Admin, maka pada halaman akun tersebut akan tersedia semua fitur, seperti fitur untuk memanajemen data buku, memanajemen data distributor, memanajemen data pasokan buku, memanajemen data pegawai, melihat laporan keuntungan dan laporan pasokan, melihat data penjualan, backup data, dan lain sebagainya.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q3">Apa itu Akses sebagai Kasir?</a>
                        </h3>
                    </div>
                    <div id="faq1_q3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Jika akun tersebut mempunyai Akses sebagai Kasir, maka halaman akun tersebut hanya tersedia beberapa fitur saja. Seperti fitur menambah transaksi penjualan, melihat data penjualan, dan mencetak struk transaksi penjualan. </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#faq1" href="#faq1_q3">Apa itu Akses sebagai Petugas?</a>
                        </h3>
                    </div>
                    <div id="faq1_q3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Jika akun tersebut mempunyai Akses sebagai Petugas, maka halaman akun tersebut hanya tersedia fitur untuk menambah data pasokan buku saja. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->

<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="text-center">
        OneBook by Rifqi Maulatur &copy;
    </div>
</footer>
<?php 
$this->load->view('layout/template_footer_start.php');
$this->load->view('layout/template_footer_end.php');
?>