<?php 
$this->load->view('layout/template_head_start');
?>

<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">

<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>
<!-- Page Content -->
<?php foreach($datanews as $row){?>
<div class="content content-boxed">
    <div class="block">
	    <div class="block-header bg-gray-lighter">
	        <ul class="block-options">
<!-- 	            <li>
	                <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"></button>
	            </li> -->
	            <li>
                            <a 
                            data-id-news="<?php echo $row->id_news ?>"
                            data-title="<?php echo $row->title ?>"
                            data-content='<?php echo $row->content ?>'
                            data-toggle="modal" data-target="#modal-data">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" data-toggle="modal" data-target="#modal-data" title="Edit"><i class="fa fa-pencil"></i></button>
                            </a>
	            </li>
	        </ul>
<!-- 	        <ul class="block-options block-options-left">
	            <li>
	                <?php

                        	echo $row->categories;
                        	echo "<br>";
                        	echo $row->type;
                        	echo "<br>";
	                ?>
	            </li>
	        </ul> -->
	    </div>
        <div class="block-content block-content-full block-content-narrow">
            <table class="table table-striped table-borderless">
                <tbody>
                    <tr>
                        <!-- <td class="hidden-xs"></td> -->
                        <td class="font-s13 text-muted">
                        	<?php echo $row->type; echo "<br>";?>
                        	Categories : <?php echo $row->categories; echo "<br><br>"; ?>
                            Published by <a><?php echo $row->name?></a> on <?php echo $this->general->humanDate($row->created_at)?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        	<div class="text-center">
                        		<h3><?php echo $row->title?> </h2> <br>
                        	</div>

                        	<?php echo $row->content;?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>
<!-- END Page Content -->

<!-- Modal Edit Data -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="modal-data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
            <ul class="block-options">
                <li>
                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Form Articles</h3>
        </div>
            <form class="js-validation-bootstrap form-horizontal" method="post" enctype="multipart/form-data" role="form" id="form">
             <div class="block-content">
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Categories</label>
                         <div class="col-lg-10">
                         <input type="text" id="id_news" name="id_news" hidden>
                          <?php
                          echo "
                          <select class='js-select2 form-control' name='id_categories' id='id_categories' style='width: 100%;' data-placeholder='Select Categories'>
                           <option value='' disabled selected></option>";
                            foreach ($datacategories as $row) {  
                            echo "<option value='".$row->id_categories."'>".$row->categories."</option>";
                            }
                            echo"
                          </select>";
                          ?>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Type</label>
                         <div class="col-lg-10">
                          <?php
                          echo "
                          <select class='js-select2 form-control' name='id_type' id='id_type' style='width: 100%;' data-placeholder='Select Type'>
                           <option value='' disabled selected></option>";
                            foreach ($datatype as $row) {  
                            echo "<option value='".$row->id_type."'>".$row->type."</option>";
                            }
                            echo"
                          </select>";
                          ?>
                          <input type="text" id="id_user" name="id_user" value="<?php echo $this->session->userdata('id_user') ?>" hidden>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Title</label>
                         <div class="col-lg-10">
                          <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Content</label>
                         <div class="col-lg-10">
                          <textarea class="form-control" id="content" name="content" style="width: 450%; height: 500px;"></textarea>
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button class="btn btn-primary" type="submit" name="submit" id="saveBtn"> Save&nbsp;</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                 </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Data -->
<?php 
$this->load->view('layout/base_footer.php');
?>
<?php 
$this->load->view('layout/template_footer_start.php');
?>

<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/ckeditor/ckeditor.js')?>"></script>

<style type="text/css">
	.my-error-class {
	    color:red;
	}
</style>
<script type="text/javascript">

	$('#modal-data').on('show.bs.modal', function (e) {
	    var div = $(e.relatedTarget)
	    var modal = $(this)
	    $('.block-title').text('Edit Articles');
	    modal.find('#id_news').attr("value",div.data('id-news'));
	    modal.find('#title').attr("value",div.data('title'));
	    var b = modal.find('#content').val(div.data('content'));
	    CKEDITOR.instances['content'].setData(b);
	    $('#form').attr('action', "<?php echo base_url('user/Articles/update'); ?>").submit();
	});


		$('#saveBtn').on('click', function(e){	

		  var validator = $("#form").validate({
		  	errorClass: "my-error-class",
		    rules: {
			  	id_categories: {required: true},
		        id_type: {required: true},
		        title: {required: true},
		        content: {

		        		required: function() 
		        		{
		        		CKEDITOR.instances.content.updateElement();
		        		}
		        }
		    },
		    messages: {
		    		  	id_categories: {required: "Please Select Categories"},
		    	        id_type: {required: "Please Select Type"},
		    	        title: {required: "Title can't be empty"},
		    	        content: {required: "Content can't be empty"}
		    },
		    errorPlacement: function(error, element) 
		    {
		        if (element.attr("name") == "content") 
		       {
		        error.insertBefore("textarea#content");
		        } else {
		        error.insertBefore(element);
		        }
		    }
		  });

	});

		//select2
		jQuery(function () {
		    // Init page helpers (Select2 plugin)
		    App.initHelpers('select2');
		});

		CKEDITOR.replace( 'content' );
</script>

<?php
$this->load->view('layout/template_footer_end.php');
