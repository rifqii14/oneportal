<?php 
$this->load->view('layout/template_head_start');
?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.css')?>">
<?php
$this->load->view('layout/template_head_end.php');
$this->load->view('layout/base_head.php');
?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
               Articles Data
            </h1>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
<!-- Dynamic Table Full -->
<div class="block">
    <div class="block-header">
        
    </div>
    <div class="block-content">
        <p class="text-muted font-13 m-b-30">
          <button id="addBtn" style="width:100px;" class="btn btn-success btn-block"><span class="icon-plus3"></span> Add Data</button>
        </p>
    <div class="table-responsive">
<?php if(!empty($datanews)){ ?>
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th class="text-center" width="7%">No.</th>
                    <th>Categories</th>
                    <th>Type</th>
                    <th>Author</th>
                    <th>Title</th>
                    <!-- <th>Content</th> -->
                    <th>Active</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php $no = 1; foreach($datanews as $row) { ?>
                <tr>
                    <td class="text-center" width="10%"><?php echo $no++?></td>
                    <td class="font-w600"><?php echo $row->categories ?></td>
                    <td class="font-w600"><?php echo $row->type ?></td>
                    <td class="font-w600"><?php echo $row->name ?></td>
                    <td class="font-w600"><?php echo $row->title ?></td>
                    <!-- <td class="font-w600"><?php echo $row->content ?></td> -->
                    <td class="font-w600"><?php echo strtoupper($row->active) ?></td>
                    <td class="text-center">
                        <div class="btn-group">
<!--                             <a 
                            data-id-news="<?php echo $row->id_news ?>"
                            data-title="<?php echo $row->title ?>"
                            data-content='<?php echo $row->content ?>'
                            data-toggle="modal" data-target="#modal-data">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" data-toggle="modal" data-target="#modal-data" title="Edit"><i class="fa fa-pencil"></i></button>
                            </a> -->
                            <a href="<?= base_url().'user/Articles/delete/'.$row->id_news ?>" class="hapus">
                            <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>
                            </a>

                            
                            <form action="<?= base_url().'user/Articles/detail' ?>" method="GET" enctype="multipart/form-data">
                              <input type="hidden" name="id" id="id" value="<?php echo $row->id_news ?>">
                              <button class="btn btn-xs btn-default" type="submit" data-toggle="tooltip" title="Detail"><i class="fa fa-eye"></i></button>
                            </form>


                        <?php
                        	if($row->active == 'no'){
                        		echo 
                        		'<a href="'.base_url().'user/Articles/active/'.$row->id_news.'" class="aktif">
                        		<button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Active"><i class="fa fa-check"></i></button>
                        		</a>';

                        	} else {
                        		echo
                        		'<a href="'.base_url().'user/Articles/deactive/'.$row->id_news.'" class="nonaktif">
                        		<button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Deactive"><i class="fa fa-power-off"></i></button>
                        		</a>';
                        	}

                        ?>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
<?php } else { ?>
<table class="table table-bordered table-striped table-vcenter js-dataTable-full">
    <thead>
        <tr>
            <th class="text-center" width="7%">No.</th>
            <th>Categories</th>
            <th>Type</th>
            <th>Author</th>
            <th>Title</th>
            <!-- <th>Content</th> -->
            <th>Active</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
  
    </tbody>
</table>
<?php } ?>
    </div>
    </div>
</div>
<!-- END Dynamic Table Full -->

<!-- Modal Edit Data -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" id="modal-data" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
            <ul class="block-options">
                <li>
                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Form Articles</h3>
        </div>
            <form class="js-validation-bootstrap form-horizontal" method="post" enctype="multipart/form-data" role="form" id="form">
             <div class="block-content">
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Categories</label>
                         <div class="col-lg-10">
                         <input type="text" id="id_news" name="id_news" hidden>
                          <?php
                          echo "
                          <select class='js-select2 form-control' name='id_categories' id='id_categories' style='width: 100%;' data-placeholder='Select Categories'>
                           <option value='' disabled selected></option>";
                            foreach ($datacategories as $row) {  
                            echo "<option value='".$row->id_categories."'>".$row->categories."</option>";
                            }
                            echo"
                          </select>";
                          ?>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Type</label>
                         <div class="col-lg-10">
                          <?php
                          echo "
                          <select class='js-select2 form-control' name='id_type' id='id_type' style='width: 100%;' data-placeholder='Select Type'>
                           <option value='' disabled selected></option>";
                            foreach ($datatype as $row) {  
                            echo "<option value='".$row->id_type."'>".$row->type."</option>";
                            }
                            echo"
                          </select>";
                          ?>
                          <input type="text" id="id_user" name="id_user" value="<?php echo $this->session->userdata('id_user') ?>" hidden>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Title</label>
                         <div class="col-lg-10">
                          <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-2 col-sm-2 control-label">Content</label>
                         <div class="col-lg-10">
                          <textarea class="form-control" id="content" name="content" style="width: 450%; height: 500px;"></textarea>
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button class="btn btn-primary" type="submit" name="submit" id="saveBtn"> Save&nbsp;</button>
                     <button type="button" class="btn btn-danger" data-dismiss="modal"> Batal</button>
                 </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Edit Data -->
</div>
<!-- END Page Content -->

<?php 
$this->load->view('layout/base_footer.php');
?>
<?php
$this->load->view('layout/template_footer_start.php');
?>
<!-- Page JS Plugins -->
<script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/sweetalert/dist/sweetalert.min.js')?>""></script>
<script src="<?php echo base_url('assets/js/pages/base_tables_datatables.js')?>"></script>
<script src="<?php echo base_url('assets/js/plugins/ckeditor/ckeditor.js')?>"></script>

<!-- Page JS Code -->
<style type="text/css">
	.my-error-class {
	    color:red;
	}
</style>
<script>

	$('#addBtn').on('click', function(e){	
		e.preventDefault();
		var modal = $('#modal-data')

	    modal.find('#id_news').attr("value","");
	    modal.find('#title').attr("value","");
	    modal.find('#content').attr("value","");
		modal.modal('show');
		$('.block-title').text('Add Articles');
		$('#form').attr('action', "<?php echo base_url('user/Articles/create'); ?>").submit();

	});

	$('#modal-data').on('show.bs.modal', function (e) {
	    var div = $(e.relatedTarget)
	    var modal = $(this)
	    $('.block-title').text('Edit Articles');
	    modal.find('#id_news').attr("value",div.data('id-news'));
	    modal.find('#title').attr("value",div.data('title'));
	    var b = modal.find('#content').val(div.data('content'));
	    CKEDITOR.instances['content'].setData(b);
	    $('#form').attr('action', "<?php echo base_url('user/Articles/update'); ?>").submit();
	});


	$('#saveBtn').on('click', function(e){	

		  var validator = $("#form").validate({
		  	errorClass: "my-error-class",
		    rules: {
			  	id_categories: {required: true},
		        id_type: {required: true},
		        title: {required: true},
		        content: {

		        		required: function() 
		        		{
		        		CKEDITOR.instances.content.updateElement();
		        		}
		        }
		    },
		    messages: {
		    		  	id_categories: {required: "Please Select Categories"},
		    	        id_type: {required: "Please Select Type"},
		    	        title: {required: "Title can't be empty"},
		    	        content: {required: "Content can't be empty"}
		    },
		    errorPlacement: function(error, element) 
		    {
		        if (element.attr("name") == "content") 
		       {
		        error.insertBefore("textarea#content");
		        } else {
		        error.insertBefore(element);
		        }
		    }
		  });

	});

    $('.hapus').on("click", function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      swal({
          title: "Yakin Ingin Hapus?",
          text: "Data yang sudah dihapus tidak dapat dikembalikan!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Ya',
          cancelButtonText: "Tidak",
          confirmButtonClass: "btn-danger",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            swal("Berhasil!", "Data berhasil dihapus!", "success");
            window.location.replace(url);
          } else {
            swal("Batal!", "Data tidak jadi terhapus!", "error");
          }
        });
    });


        //select2
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            App.initHelpers('select2');
        });

        CKEDITOR.replace( 'content' );
</script>
<?php
$this->load->view('layout/template_footer_end.php');
?>