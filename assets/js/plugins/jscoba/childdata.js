
var ChildData = function(element, idName, addElement) {
	this.element = element;
	this.data = [];
	this.delFunc = null;
	this.idName = idName;
	if(addElement)
		this.addElement = addElement;
	this.start();
};

ChildData.prototype.start = function() {
	var size = $(this.element + " > thead th").length;
	var $row = $("<tr/>");
	
	if(this.data.length == 0) {
		$row.append($("<td colspan='" + size + "' style='text-align:center;' id='row-nodata' />").html("No Data to Display"));
		$(this.element ).append($row);
	} else {
		$row = $('#row-nodata');
		$row.remove();
	}
}

ChildData.prototype.setDelFunc = function(func) {
	this.delFunc = func;
	//console.log(this.delFunc);
};

ChildData.prototype.setAddEle = function(el) {
	this.addElement = el;
};

ChildData.prototype.setAddFunc = function(data, callback) {
	var self = this;
	
	$(this.addElement).on('click', function() {
		self.add(data, callback);
	});
	
}

// Used to Add new data to the old data
ChildData.prototype.add = function(datas, callback) {
	var self = this;
	if(datas[this.idName] === null || datas[this.idName] === undefined ) {
		datas[this.idName] = this.data.length + 1;
	}
	//console.log(datas);
	this.data.push(datas);
	self.start();
	//Creating Row for TableBody
	var $row = $("<tr/>");

	//Iterate Row and Create Column(TD) inside that
	$.each(datas, function(key, val) {
		if(key !== self.idName)
	    	$row.append($("<td/>").html(val));
	    else
	    	$row.append($('<input type="hidden" name="child_id" id="child-id" />').val(val));
	});

	//Append Delete Button to the Row
	var $button = $('<input type="button" id="remove" value="Remove" class="btn btn-block btn-danger btn-child" data-id = "'+ datas[self.idName] + '" />');
	$row.append($("<td />").html($button)); 
	
	//Append Row to tablebody
	$(this.element).append($row);
	var btnfunc = this.delFunc;

	//Append Delete Event for Delete Button
	$button.on("click", function() {
		self.remove($(this).data('id'), btnfunc(datas));
	});

	if (typeof callback === "function") {
		callback();
	}
};

ChildData.prototype.remove = function(id, callback) {
	var ob;
	var idname = this.idName;
	var index = $.map(this.data, function(obj, index) {
				    if(obj[idname] === id) {
				    	ob = obj;
				        return index;
				    }
				});
	if(index > -1) {
		this.data.splice(index, 1);
		$('.btn-child[data-id="'+id+'"]').parents('tr').remove();
		this.start();
	}
	if(typeof callback === "function") {
		//callback().call(ob.name);
	}
};

ChildData.prototype.setEmpty = function() {
	this.data = [];
};

ChildData.prototype.printData = function() {
};

ChildData.prototype.getData = function() {
	return this.data;
};