-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2017 at 04:02 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `141510300_ukom`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_buku`
--

CREATE TABLE IF NOT EXISTS `tbl_buku` (
  `id_buku` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `noisbn` varchar(255) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `penerbit` varchar(255) NOT NULL,
  `tahun` year(4) NOT NULL,
  `stok` int(11) NOT NULL DEFAULT '0',
  `harga_pokok` double NOT NULL,
  `harga_jual` double NOT NULL,
  `ppn` decimal(4,2) NOT NULL,
  `diskon` decimal(5,2) NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_buku`),
  KEY `id_diskon` (`diskon`),
  KEY `id_kategori` (`id_kategori`),
  KEY `diskon` (`diskon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tbl_buku`
--

INSERT INTO `tbl_buku` (`id_buku`, `id_kategori`, `judul`, `cover`, `noisbn`, `penulis`, `penerbit`, `tahun`, `stok`, `harga_pokok`, `harga_jual`, `ppn`, `diskon`, `create`, `update`, `delete`) VALUES
(1, 1, 'Perempuan dan Hak Warisnya', 'file_1485270573.jpg', '9786021683040', 'Prof. Dr. Thaha Dasuqi Hubaisyi', 'Tatban ', 2016, 17, 45000, 47025, 0.10, 0.95, '2017-01-19 22:46:52', '2017-01-29 11:52:27', NULL),
(2, 2, 'Keajaiban Operasi Plastik Korea Selatan', 'file_1485270481.jpg', '9789790561564', 'Agnes Davonar', 'Falcon ', 2016, 47, 50000, 49500, 0.10, 0.90, '2017-01-19 23:08:06', '2017-01-29 12:35:38', NULL),
(3, 3, 'Kreatif Mengolah Oncom, Tempe & Tahu ', 'file_1485270541.jpg', '9789790822764', 'Kirana Wijaya', 'Demedia Pustaka ', 2016, 5, 65000, 71500, 0.10, 1.00, '2017-01-20 08:22:49', '2017-01-29 11:54:55', NULL),
(4, 4, 'Kamus Sains - Panduan Praktis Berbagai Istilah Ilmu Pengetahuan dan Teknologi  ', 'file_1485273614.jpg', '9797752224', 'Wahyu Untara', 'Indonesia Tera ', 2014, 19, 46500, 51150, 0.10, 1.00, '2017-01-24 23:00:14', '2017-01-29 11:55:41', NULL),
(25, 5, 'The Best Of Catatan Dodol Calon Dokter  ', 'file_1485274489.jpg', '9786022201984', 'Ferdiriva Hamzah', 'Bukune ', 2016, 18, 90000, 89100, 0.10, 0.90, '2017-01-24 23:14:49', '2017-01-29 11:57:11', NULL),
(37, 1, 'Sejarah Daulah Umawiyah & Abbasiyah', 'file_1485282575.jpg', '9786027637542', 'Dr. Ali Muhammad Ash-Shallabi', 'UMMUL QURO', 2017, 35, 55000, 57475, 0.10, 0.95, '2017-01-25 01:29:35', '2017-01-29 11:57:41', NULL),
(39, 16, 'LC: Lucky Luke - Sang Pengawal  ', 'file_1485317952.jpg', '9786020281278', 'Morris', 'Elex Media Komputindo ', 2016, 12, 20000, 22000, 0.10, 1.00, '2017-01-25 11:19:12', '2017-01-29 12:37:05', NULL),
(40, 3, 'CharPao the Next Level Bakpao Lezat dengan Aneka Karakter Lucu', 'file_1485318317.jpg', '9786020295091', 'Diana Cahya', 'Elex Media Komputindo ', 2016, 18, 35000, 28875, 0.10, 0.75, '2017-01-25 11:25:17', '2017-02-04 01:35:52', NULL),
(42, 2, 'Nabi Saja Suka Buah', 'file_1485318771.jpg', '9789790390133', 'Dr. Sunardi', 'AQWAM MEDIKA ', 2015, 12, 40000, 41800, 0.10, 0.95, '2017-01-25 11:32:51', '2017-01-29 11:58:42', NULL),
(43, 20, 'Indonesian Muslimah Fashion Blogger Now', 'file_1485670979.jpg', '9786020310374', 'Ade Aprilia', 'Gramedia Pustaka Utama ', 2016, 0, 150000, 156750, 0.10, 0.95, '2017-01-29 13:22:59', NULL, NULL),
(44, 23, 'Ayah Ibu Baik - Parenting Era Digital', 'file_1486147479.jpg', '9786023880317', 'Ir. Jarot Wijanarko, N.Pd & Ir. Ester Setiawati, M.Pd.', 'Happy Holy Kids', 2016, 3, 50000, 55000, 0.10, 1.00, '2017-02-04 01:44:39', '2017-02-05 10:00:47', NULL),
(45, 1, 'Juz Amma & Al-Ma Tsurat', 'file_1486263972.jpg', '9786020297538', 'Tim Quanta', 'Elex Media Komputindo', 2017, 9, 95000, 104500, 0.10, 1.00, '2017-02-05 10:06:12', '2017-02-05 10:08:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_diskon`
--

CREATE TABLE IF NOT EXISTS `tbl_diskon` (
  `diskon` decimal(5,2) NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`diskon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_diskon`
--

INSERT INTO `tbl_diskon` (`diskon`, `create`, `update`, `delete`) VALUES
(0.40, '2017-02-04 01:39:29', NULL, NULL),
(0.50, '2017-01-19 07:36:15', NULL, NULL),
(0.55, '2017-01-19 07:36:47', NULL, NULL),
(0.60, '2017-01-19 07:36:15', NULL, NULL),
(0.65, '2017-01-19 07:34:59', NULL, NULL),
(0.70, '2017-01-19 07:34:59', NULL, NULL),
(0.75, '2017-01-19 07:34:59', NULL, NULL),
(0.80, '2017-01-19 07:34:20', NULL, NULL),
(0.85, '2017-01-19 07:34:20', NULL, NULL),
(0.90, '2017-01-19 07:33:27', NULL, NULL),
(0.93, '2017-02-05 10:02:30', NULL, NULL),
(0.95, '2017-01-19 07:33:06', NULL, NULL),
(1.00, '2017-01-24 21:39:19', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_distributor`
--

CREATE TABLE IF NOT EXISTS `tbl_distributor` (
  `id_distributor` int(11) NOT NULL AUTO_INCREMENT,
  `nama_distributor` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_distributor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_distributor`
--

INSERT INTO `tbl_distributor` (`id_distributor`, `nama_distributor`, `alamat`, `telepon`, `create`, `update`, `delete`) VALUES
(1, 'Gramedia Asri Media, HUHU', 'Perintis Building, 4th Floor, Jl. Kebahagiaan No. 4-14, Jakarta Barat, DKI Jakarta', '0212601234', '2017-01-18 23:59:56', '2017-02-05 19:10:51', NULL),
(2, 'Gramedia Asri Media, PT [Yogyakarta Branch]', 'Jl. Jend. Sudirman No. 54-56, Yogyakarta, DI Yogyakarta', '0274512621', '2017-01-19 23:09:55', NULL, NULL),
(5, 'Express, CV', 'Jl. Ahmad Jais No. 7, Surabaya, Jawa Timur 60274', '0315342319', '2017-01-24 01:41:10', NULL, NULL),
(7, 'Distributor Buku Kita [Jabodetabek]', ' Jl. Kelapa Hijau No. 22 Rt 006/03 Jagakarsa, Jakarta Selatan 12620 Indonesia', '02178881850', '2017-01-24 01:47:19', NULL, NULL),
(8, 'Distributor dan Toko Buku Setia Kawan', 'Jl. Kramat Raya No.3L, RT.4/RW.2, Kramat, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10450', '0213155242', '2017-01-25 01:39:54', '2017-01-25 20:54:26', NULL),
(9, 'A', 'A', '1', '2017-02-04 01:38:10', NULL, '2017-02-04 01:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kasir`
--

CREATE TABLE IF NOT EXISTS `tbl_kasir` (
  `id_kasir` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `status` enum('Aktif','Tidak Aktif') NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `akses` enum('Admin','Kasir','Petugas') NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kasir`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbl_kasir`
--

INSERT INTO `tbl_kasir` (`id_kasir`, `nama`, `alamat`, `telepon`, `status`, `username`, `password`, `akses`, `create`, `update`, `delete`) VALUES
(1, 'Muhammad Rifqi', 'Jalan Jatibarang 7 No. 48, Pulogadung, Jakarta Timur', '08999764787', 'Aktif', 'rifqi@cashier', '2f8b2dbb9565e31772a0f5ffe9736064', 'Kasir', '2017-01-19 22:56:31', '2017-01-25 20:06:14', NULL),
(17, 'Fariz Yarham', 'Jalan Letnan Arsyad Raya, No. 20 RT 05/20, Kayuringin Jaya, Bekasi Selatan', '085760008130', 'Aktif', 'fariz@cashier', '76c64f96f8e344cfdbad5a0bba5bb3d5', 'Kasir', '2017-01-24 15:37:38', NULL, '2017-01-24 15:46:02'),
(18, 'Admin', 'Jalan Beringin 3, RT 10/05, Kranji, Bekasi Barat', '08998246948', 'Aktif', 'admin@admin', 'a3175a452c7a8fea80c62a198a40f6c9', 'Admin', '2017-01-25 01:42:36', '2017-01-29 12:00:04', NULL),
(20, 'Tsabit Ghazwan', 'Jalan Wijaya Kusuma 8, RT 05/02, Jakasampurna, Bekasi Barat', '08876673612', 'Aktif', 'tsabit@cashier', '82f957dff7d5f50dbd76e6165597622c', 'Kasir', '2017-01-29 21:06:00', '2017-02-01 05:10:00', NULL),
(21, 'Nabilah Ratna Ayu', 'Jalan Hanjuang 7 No. 89 RT 03/02, Jatibening, Bekasi Selatan', '081387892290', 'Aktif', 'nabilah@cashier', '895dee36b101ae8ad0ec0709b896a9f8', 'Kasir', '2017-02-04 00:57:08', '2017-02-05 09:06:03', NULL),
(24, 'Ian Andri', 'Jl. Mas Koki Raya RT 05/10, Jati, Pulo Gadung, Kota Jakarta Timur', '081976781290', 'Aktif', 'ian@officer', '3e1af117ca769d8b57351e628a43f4d0', 'Petugas', '2017-02-05 09:39:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `kategori`, `create`, `update`, `delete`) VALUES
(1, 'Agama', '2017-01-19 22:42:32', NULL, NULL),
(2, 'Kesehatan dan Lingkungan', '2017-01-19 23:00:48', NULL, NULL),
(3, 'Masakan dan Makanan', '2017-01-20 08:06:58', NULL, NULL),
(4, 'Sains dan Teknologi', '2017-01-23 23:58:07', NULL, NULL),
(5, 'Humor', '2017-01-24 00:00:06', NULL, NULL),
(6, 'Sejarah dan Budaya', '2017-01-25 01:32:04', '2017-01-25 10:19:46', NULL),
(16, 'Komik', '2017-01-25 11:15:45', '2017-01-25 20:29:20', NULL),
(20, 'Busana dan Kecantikan', '2017-01-29 13:20:32', '2017-01-29 13:20:40', NULL),
(23, 'Orang Tua dan Keluarga', '2017-02-04 01:42:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pasok`
--

CREATE TABLE IF NOT EXISTS `tbl_pasok` (
  `id_pasok` int(11) NOT NULL AUTO_INCREMENT,
  `id_distributor` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pasok`),
  KEY `id_distributor` (`id_distributor`),
  KEY `id_distributor_2` (`id_distributor`),
  KEY `id_buku` (`id_buku`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `tbl_pasok`
--

INSERT INTO `tbl_pasok` (`id_pasok`, `id_distributor`, `id_buku`, `jumlah`, `tanggal`, `create`, `update`, `delete`) VALUES
(18, 1, 1, 21, '2017-01-27 23:23:23', '2017-01-27 23:23:23', NULL, NULL),
(19, 1, 42, 60, '2017-02-09 07:20:12', '2017-02-09 07:20:12', NULL, NULL),
(20, 5, 2, 50, '2017-02-01 00:00:00', '2017-02-01 00:00:00', '2017-01-28 00:37:41', NULL),
(21, 2, 4, 25, '2017-01-28 00:10:22', '2017-01-28 00:10:22', NULL, NULL),
(22, 8, 37, 40, '2017-01-28 18:56:01', '2017-01-28 18:56:01', NULL, NULL),
(23, 1, 3, 10, '2017-01-28 18:56:26', '2017-01-28 18:56:26', NULL, NULL),
(24, 7, 40, 5, '2017-01-28 18:56:54', '2017-01-28 18:56:54', NULL, NULL),
(25, 1, 25, 20, '2017-01-29 12:31:08', '2017-01-29 12:31:08', NULL, NULL),
(26, 7, 39, 15, '2017-01-29 12:31:22', '2017-01-29 12:31:22', NULL, NULL),
(27, 7, 39, 6, '2017-01-29 12:36:25', '2017-01-29 12:36:25', '2017-01-29 12:37:21', NULL),
(28, 1, 42, 5, '2017-01-29 13:04:28', '2017-01-29 13:04:28', NULL, NULL),
(29, 1, 42, 12, '2017-01-29 13:05:16', '2017-01-29 13:05:16', NULL, NULL),
(30, 1, 43, 5, '2017-01-29 13:23:28', '2017-01-29 13:23:28', NULL, NULL),
(31, 7, 40, 1, '2017-01-30 21:20:10', '2017-01-30 21:20:10', '2017-01-30 21:34:06', NULL),
(32, 7, 40, 19, '2017-02-04 01:35:18', '2017-02-04 01:35:18', '2017-02-04 01:35:52', NULL),
(33, 2, 44, 3, '2017-02-05 10:00:29', '2017-02-05 10:00:29', '2017-02-05 10:01:07', NULL),
(34, 1, 45, 10, '2017-02-05 10:09:31', '2017-02-05 10:09:31', NULL, NULL);

--
-- Triggers `tbl_pasok`
--
DROP TRIGGER IF EXISTS `update_tambah_kurang_stok_buku`;
DELIMITER //
CREATE TRIGGER `update_tambah_kurang_stok_buku` AFTER UPDATE ON `tbl_pasok`
 FOR EACH ROW BEGIN
IF OLD.jumlah < NEW.jumlah
THEN
UPDATE tbl_buku SET stok = (stok+(NEW.jumlah-OLD.jumlah))
WHERE id_buku = NEW.id_buku;
ELSEIF OLD.jumlah > NEW.jumlah
THEN 
UPDATE tbl_buku SET stok = (stok-(OLD.jumlah-NEW.jumlah)),
`tbl_buku`.`update` = now()
WHERE id_buku = NEW.id_buku;
END if;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_tambah_stok_buku`;
DELIMITER //
CREATE TRIGGER `update_tambah_stok_buku` AFTER INSERT ON `tbl_pasok`
 FOR EACH ROW BEGIN
UPDATE tbl_buku SET stok = (stok+(NEW.jumlah))
WHERE id_buku = NEW.id_buku;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penjualan`
--

CREATE TABLE IF NOT EXISTS `tbl_penjualan` (
  `id_penjualan` int(11) NOT NULL AUTO_INCREMENT,
  `no_referensi` varchar(8) NOT NULL,
  `id_kasir` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penjualan`),
  UNIQUE KEY `no_referensi` (`no_referensi`),
  KEY `id_kasir` (`id_kasir`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `tbl_penjualan`
--

INSERT INTO `tbl_penjualan` (`id_penjualan`, `no_referensi`, `id_kasir`, `tanggal`, `create`, `update`, `delete`) VALUES
(6, 'ABCD1234', 1, '2017-01-29 12:10:37', '2017-01-29 12:10:37', NULL, NULL),
(7, 'WXYZ1234', 1, '2017-01-02 09:00:00', '2017-01-02 09:00:00', NULL, NULL),
(9, 'EFGH1234', 1, '2017-01-01 11:00:00', '2017-01-01 11:00:00', NULL, NULL),
(10, 'IJKL1234', 17, '2017-02-03 12:00:00', '2017-03-03 12:00:00', NULL, NULL),
(11, 'CVBN4321', 20, '2017-02-01 11:42:58', '2017-02-01 11:42:58', NULL, NULL),
(22, 'ZTGZZ6SZ', 17, '2017-02-04 00:43:21', '0000-00-00 00:00:00', NULL, NULL),
(27, '281Y94YA', 21, '2017-02-04 01:07:23', '2017-02-04 01:07:23', NULL, NULL),
(34, 'ZEGMZOVV', 1, '2017-02-04 01:21:28', '2017-02-04 01:21:28', NULL, NULL),
(35, 'FWBGR6PZ', 20, '2017-02-04 02:05:01', '2017-02-04 02:05:01', NULL, NULL),
(43, 'X50JGMKE', 20, '2017-02-04 22:37:34', '2017-02-04 22:37:34', NULL, NULL),
(75, 'CRV64Y0F', 1, '2017-02-04 23:26:35', '2017-02-04 23:26:35', NULL, NULL),
(80, '35GMWZB2', 17, '2017-02-05 00:06:05', '2017-02-05 00:06:05', NULL, NULL),
(81, 'THX8AVVZ', 21, '2017-02-05 10:10:47', '2017-02-05 10:10:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penjualan_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_penjualan_detail` (
  `id_penjualan_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_penjualan` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime DEFAULT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_penjualan_detail`),
  KEY `id_penjualan` (`id_penjualan`,`id_buku`),
  KEY `id_buku` (`id_buku`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `tbl_penjualan_detail`
--

INSERT INTO `tbl_penjualan_detail` (`id_penjualan_detail`, `id_penjualan`, `id_buku`, `jumlah`, `subtotal`, `create`, `update`, `delete`) VALUES
(6, 6, 2, 1, 49500, '2017-01-29 12:13:48', NULL, NULL),
(7, 6, 39, 2, 44000, '2017-01-29 12:13:48', NULL, NULL),
(8, 7, 25, 1, 89100, '2017-01-29 12:14:57', NULL, NULL),
(9, 7, 3, 1, 71500, '2017-01-29 12:14:57', NULL, NULL),
(11, 9, 40, 1, 28875, '2017-01-29 12:16:46', NULL, NULL),
(12, 9, 39, 5, 110000, '2017-01-29 12:38:16', NULL, NULL),
(13, 9, 42, 58, 2424400, '2017-01-29 12:52:04', NULL, NULL),
(14, 10, 40, 3, 86625, '2017-01-29 13:08:59', NULL, NULL),
(16, 11, 43, 4, 627000, '2017-02-01 11:53:31', NULL, NULL),
(17, 11, 2, 1, 49500, '2017-02-04 00:00:00', NULL, NULL),
(18, 11, 4, 1, 51150, '2017-02-04 00:00:00', NULL, NULL),
(19, 11, 42, 1, 41800, '2017-02-04 00:04:22', NULL, NULL),
(20, 11, 3, 1, 71500, '2017-02-04 00:33:29', NULL, NULL),
(21, 11, 1, 1, 47025, '2017-02-04 00:33:29', NULL, NULL),
(30, 22, 40, 1, 28875, '2017-02-04 00:43:21', NULL, NULL),
(31, 22, 25, 1, 89100, '2017-02-04 00:43:21', NULL, NULL),
(36, 27, 4, 2, 102300, '2017-02-04 01:07:23', NULL, NULL),
(37, 27, 37, 1, 57475, '2017-02-04 01:07:23', NULL, NULL),
(38, 27, 40, 1, 28875, '2017-02-04 01:07:23', NULL, NULL),
(39, 34, 43, 1, 156750, '2017-02-04 01:21:28', NULL, NULL),
(40, 34, 3, 1, 71500, '2017-02-04 01:21:28', NULL, NULL),
(41, 34, 39, 2, 44000, '2017-02-04 01:21:28', NULL, NULL),
(42, 35, 4, 1, 51150, '2017-02-04 02:05:01', NULL, NULL),
(43, 35, 42, 2, 83600, '2017-02-04 02:05:01', NULL, NULL),
(44, 43, 3, 1, 71500, '2017-02-04 22:37:34', NULL, NULL),
(45, 43, 42, 2, 83600, '2017-02-04 22:37:34', NULL, NULL),
(46, 75, 1, 1, 47025, '2017-02-04 23:26:35', NULL, NULL),
(47, 75, 2, 1, 49500, '2017-02-04 23:26:35', NULL, NULL),
(48, 75, 3, 1, 71500, '2017-02-04 23:26:35', NULL, NULL),
(49, 75, 4, 1, 51150, '2017-02-04 23:26:35', NULL, NULL),
(50, 75, 25, 1, 89100, '2017-02-04 23:26:35', NULL, NULL),
(51, 75, 37, 1, 57475, '2017-02-04 23:26:35', NULL, NULL),
(52, 75, 39, 1, 22000, '2017-02-04 23:26:35', NULL, NULL),
(53, 75, 40, 1, 28875, '2017-02-04 23:26:35', NULL, NULL),
(54, 75, 42, 1, 41800, '2017-02-04 23:26:35', NULL, NULL),
(55, 80, 39, 1, 22000, '2017-02-05 00:06:05', NULL, NULL),
(56, 81, 45, 1, 104500, '2017-02-05 10:10:47', NULL, NULL);

--
-- Triggers `tbl_penjualan_detail`
--
DROP TRIGGER IF EXISTS `update_kurang_stok_buku`;
DELIMITER //
CREATE TRIGGER `update_kurang_stok_buku` AFTER INSERT ON `tbl_penjualan_detail`
 FOR EACH ROW BEGIN
UPDATE tbl_buku as a set stok = stok - NEW.jumlah
WHERE a.id_buku = NEW.id_buku and a.id_buku > 0 && NEW.jumlah <= a.stok;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_struk`
--

CREATE TABLE IF NOT EXISTS `tbl_struk` (
  `id_struk` int(11) NOT NULL AUTO_INCREMENT,
  `id_penjualan` int(11) NOT NULL,
  `bayar` double NOT NULL,
  `kembalian` double NOT NULL,
  `create` datetime NOT NULL,
  `update` datetime NOT NULL,
  `delete` datetime DEFAULT NULL,
  PRIMARY KEY (`id_struk`),
  UNIQUE KEY `no_referensi` (`id_penjualan`),
  KEY `id_penjualan` (`id_penjualan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_pasok_perbuku`
--
CREATE TABLE IF NOT EXISTS `v_pasok_perbuku` (
`id_buku` int(11)
,`judul` varchar(255)
,`nama_distributor` varchar(255)
,`pasok_perbuku` decimal(32,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_penjualan`
--
CREATE TABLE IF NOT EXISTS `v_penjualan` (
`id_penjualan` int(11)
,`no_referensi` varchar(8)
,`nama` varchar(255)
,`tanggal` datetime
,`total` double
,`delete` datetime
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_totalpasok`
--
CREATE TABLE IF NOT EXISTS `v_totalpasok` (
`total_pasok` decimal(32,0)
,`tahun` int(4)
,`bulan` varchar(9)
,`no_bulan` int(2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_profit_perbulan`
--
CREATE TABLE IF NOT EXISTS `v_profit_perbulan` (
`tahun` int(4)
,`bulan` varchar(9)
,`no_bulan` int(2)
,`keuntungan` decimal(45,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_profit_perhari`
--
CREATE TABLE IF NOT EXISTS `v_profit_perhari` (
`keuntungan` decimal(45,0)
,`tahun` int(4)
,`bulan` varchar(9)
,`no_bulan` int(2)
,`tanggal` int(2)
,`hari` varchar(9)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_untung_perpenjualan`
--
CREATE TABLE IF NOT EXISTS `v_untung_perpenjualan` (
`id_penjualan` int(11)
,`id_penjualan_detail` int(11)
,`id_buku` int(11)
,`jumlah` int(11)
,`tanggal` datetime
,`untung_perbuku` double
);
-- --------------------------------------------------------

--
-- Structure for view `v_pasok_perbuku`
--
DROP TABLE IF EXISTS `v_pasok_perbuku`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pasok_perbuku` AS select `c`.`id_buku` AS `id_buku`,`c`.`judul` AS `judul`,`b`.`nama_distributor` AS `nama_distributor`,sum(`a`.`jumlah`) AS `pasok_perbuku` from ((`tbl_pasok` `a` join `tbl_distributor` `b` on((`a`.`id_distributor` = `b`.`id_distributor`))) join `tbl_buku` `c` on((`a`.`id_buku` = `c`.`id_buku`))) group by `a`.`id_buku` order by sum(`a`.`jumlah`) desc;

-- --------------------------------------------------------

--
-- Structure for view `v_penjualan`
--
DROP TABLE IF EXISTS `v_penjualan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_penjualan` AS select `a`.`id_penjualan` AS `id_penjualan`,`a`.`no_referensi` AS `no_referensi`,`b`.`nama` AS `nama`,`a`.`tanggal` AS `tanggal`,sum(`c`.`subtotal`) AS `total`,`a`.`delete` AS `delete` from ((`tbl_penjualan` `a` join `tbl_penjualan_detail` `c` on((`a`.`id_penjualan` = `c`.`id_penjualan`))) join `tbl_kasir` `b` on((`a`.`id_kasir` = `b`.`id_kasir`))) group by `a`.`id_penjualan` order by `a`.`tanggal` desc;

-- --------------------------------------------------------

--
-- Structure for view `v_totalpasok`
--
DROP TABLE IF EXISTS `v_totalpasok`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_totalpasok` AS select sum(`a`.`jumlah`) AS `total_pasok`,year(`a`.`tanggal`) AS `tahun`,monthname(`a`.`tanggal`) AS `bulan`,month(`a`.`tanggal`) AS `no_bulan` from `tbl_pasok` `a` group by monthname(`a`.`tanggal`),year(`a`.`tanggal`) order by month(`a`.`tanggal`);

-- --------------------------------------------------------

--
-- Structure for view `v_profit_perbulan`
--
DROP TABLE IF EXISTS `v_profit_perbulan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_profit_perbulan` AS select year(`a`.`tanggal`) AS `tahun`,monthname(`a`.`tanggal`) AS `bulan`,month(`a`.`tanggal`) AS `no_bulan`,sum(cast((`a`.`untung_perbuku` * `a`.`jumlah`) as unsigned)) AS `keuntungan` from `v_untung_perpenjualan` `a` group by monthname(`a`.`tanggal`),year(`a`.`tanggal`) order by month(`a`.`tanggal`);

-- --------------------------------------------------------

--
-- Structure for view `v_profit_perhari`
--
DROP TABLE IF EXISTS `v_profit_perhari`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_profit_perhari` AS select sum(cast((`a`.`untung_perbuku` * `a`.`jumlah`) as unsigned)) AS `keuntungan`,year(`a`.`tanggal`) AS `tahun`,monthname(`a`.`tanggal`) AS `bulan`,month(`a`.`tanggal`) AS `no_bulan`,dayofmonth(`a`.`tanggal`) AS `tanggal`,dayname(`a`.`tanggal`) AS `hari` from `v_untung_perpenjualan` `a` group by monthname(`a`.`tanggal`),year(`a`.`tanggal`),`a`.`tanggal` order by month(`a`.`tanggal`),dayofmonth(`a`.`tanggal`);

-- --------------------------------------------------------

--
-- Structure for view `v_untung_perpenjualan`
--
DROP TABLE IF EXISTS `v_untung_perpenjualan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_untung_perpenjualan` AS select `c`.`id_penjualan` AS `id_penjualan`,`a`.`id_penjualan_detail` AS `id_penjualan_detail`,`a`.`id_buku` AS `id_buku`,`a`.`jumlah` AS `jumlah`,`c`.`tanggal` AS `tanggal`,(case when ((`b`.`harga_jual` - `b`.`harga_pokok`) < 0) then 0 else (`b`.`harga_jual` - `b`.`harga_pokok`) end) AS `untung_perbuku` from ((`tbl_penjualan_detail` `a` join `tbl_buku` `b` on((`a`.`id_buku` = `b`.`id_buku`))) join `tbl_penjualan` `c` on((`a`.`id_penjualan` = `c`.`id_penjualan`))) order by (case when ((`b`.`harga_jual` - `b`.`harga_pokok`) < 0) then 0 else (`b`.`harga_jual` - `b`.`harga_pokok`) end);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_buku`
--
ALTER TABLE `tbl_buku`
  ADD CONSTRAINT `tbl_buku_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori` (`id_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_buku_ibfk_2` FOREIGN KEY (`diskon`) REFERENCES `tbl_diskon` (`diskon`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pasok`
--
ALTER TABLE `tbl_pasok`
  ADD CONSTRAINT `tbl_pasok_ibfk_1` FOREIGN KEY (`id_distributor`) REFERENCES `tbl_distributor` (`id_distributor`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pasok_ibfk_2` FOREIGN KEY (`id_buku`) REFERENCES `tbl_buku` (`id_buku`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_penjualan`
--
ALTER TABLE `tbl_penjualan`
  ADD CONSTRAINT `tbl_penjualan_ibfk_1` FOREIGN KEY (`id_kasir`) REFERENCES `tbl_kasir` (`id_kasir`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_penjualan_detail`
--
ALTER TABLE `tbl_penjualan_detail`
  ADD CONSTRAINT `tbl_penjualan_detail_ibfk_1` FOREIGN KEY (`id_buku`) REFERENCES `tbl_buku` (`id_buku`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_penjualan_detail_ibfk_2` FOREIGN KEY (`id_penjualan`) REFERENCES `tbl_penjualan` (`id_penjualan`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_struk`
--
ALTER TABLE `tbl_struk`
  ADD CONSTRAINT `tbl_struk_ibfk_1` FOREIGN KEY (`id_penjualan`) REFERENCES `tbl_penjualan` (`id_penjualan`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
