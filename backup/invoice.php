<div class="content">
 <div class="col-lg-12">
    <div id="detail">
     <!-- Bordered Table -->
     <div class="block">
         <div class="block-header">
             <h1 class="block-title hidden" style="text-align: center">STRUK - TOKO BUKU ONEBOOK</h1>
             <h3 class="block-title" style="text-align: center">Detail Penjualan</h3>
         </div>
         <div class="block-content">
             <table border="1" class="table table-bordered" style=" margin-left: auto; margin-right: auto;">
                 <tbody>
                     <tr>
                         <td>No. Referensi : </td><td><?php echo $order['no_referensi']?></td>
                     </tr>
                     <tr>
                         <td>Nama Kasir : </td><td><?php echo $order['nama']?></td>
                     </tr>
                     <tr>
                         <td>Tanggal Order : </td><td><?php echo $this->general->humanDate($order['tanggal'])?></td>
                     </tr>
                     <tr>
                         <td>Total : </td><td><?php echo "Rp. ".number_format($order['total'])?></td>
                     </tr>
                 </tbody>
             </table>
         </div>
     </div>
     <div class="block">
         <div class="block-header">
             <h3 class="block-title" style="text-align: center">Detail Items</h3>
         </div>
         <div class="block-content">
             <table border="1" class="table table-bordered" style=" margin-left: auto; margin-right: auto;">
	             <thead>
	                 <tr>
	                     <th class="text-center">Judul Buku</th>
	                     <th class="text-center">Jumlah</th>
	                     <th class="text-center">Harga</th>
	                     <th class="text-center">Subtotal</th>
	                 </tr>
	             </thead>
                 <tbody>
                 <?php  ?>

                 <?php foreach ($orderDetails as $orderDetail){ ?>
                     <tr>

                         <td><?php echo $orderDetail['judul']; ?></td>
                         <td style="text-align:center"><?php echo $orderDetail['jumlah'] ?></td>
                         <td style="text-align:right"><?php echo "Rp. ".number_format($orderDetail['harga_jual']) ?></td>
                         <td style="text-align:right"><?php echo "Rp. ".number_format($orderDetail['subtotal']) ?></td>

                     </tr>
                 <?php } ?>
                 <tr>
                     <td colspan="3" class="font-w600"><span class="text-danger">TOTAL:</span></td>
                     <td style="text-align:right"><?php echo "Rp. ".number_format($order['total']); ?></td>                
                 </tr> 
                 </tbody>
             </table>
             <div class="hidden">
                 <center><h3>Terima Kasih</h3></center><p>
                 <center><h3>Atas Kunjungan Anda & Selamat Berbelanja Kembali</h3></center><p>
                 <center><h4>Kritik dan Saran : onebook@gmail.com</h4></center><p>
                 <center><h4>Telepon : 021-47860858</h4></center><p>
             </div>
         </div>
     </div>
     <!-- END Bordered Table -->
     </div>
     <div class="block">
        <div class="block-content">
            <div class="form-group">
                <button class="btn btn-primary btn-block" id="print">CETAK STRUK</button>
            </div>
        </div>
    </div>
 </div>